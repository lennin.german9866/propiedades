<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propiedades extends CI_Controller{
	// define contructor de la clase
  public function __construct(){
		parent::  __construct();
		$this->load->model("propiedade");
	}
//renderisa vista index estudiantes
	public function index()
	{
    $data["listadoPropiedades"]=$this->propiedade->obtenerTodos();

		$this->load->view('header');
		$this->load->view('propiedades/index',$data);
		$this->load->view('footer');
	}
	//renderis vista index nuevo
  public function nuevo()  {
  $this->load->view('header');
  $this->load->view('propiedades/nuevo');
  $this->load->view('footer');
  }

  // funcionn> para capturar los valores formulario Nuevo
  //formilario Nuevo
  public function guardarPropiedades(){
      $datosNuevoPropiedades=array(
    		 "cedula_pro"=>$this->input->post("cedula_pro"),
    		 "apellidos_pro"=>$this->input->post("apellidos_pro"),
    		 "nombres_pro"=>$this->input->post("nombres_pro"),
    		 "celular_pro"=>$this->input->post("celular_pro"),
    		 "email_pro"=>$this->input->post("email_pro"),
    		 "latitud1_pro"=>$this->input->post("latitud1_pro"),
         "longitud1_pro"=>$this->input->post("longitud1_pro"),
    		 "latitud2_pro"=>$this->input->post("latitud2_pro"),
    		 "longitud2_pro"=>$this->input->post("longitud2_pro"),
    		 "latitud3_pro"=>$this->input->post("latitud3_pro"),
    		 "longitud3_pro"=>$this->input->post("longitud3_pro"),
    		 "latitud4_pro"=>$this->input->post("latitud4_pro"),
         "longitud4_pro"=>$this->input->post("longitud4_pro")
    	);

    print_r($datosNuevoPropiedades);
    if ($this->propiedade->insertar($datosNuevoPropiedades)) {
    	redirect("propiedades/index");
    }else{
      echo "NO SE GUARDO LA INFORMACIÓN<h1>";
    }
  }
  // generear una funcion  para eliminar Estudiantes
  public function borrar($id_pro){
    if ($this->propiedade->eliminarPorId($id_pro)) {
      redirect("propiedades/index");
      }else {
      echo "<h1>Error al eliminar<h1>";
    }
  }
  //funcion para renderizar actualizacion formulario
  public function actualizar($id){
    $data["propiedadeEditar"]=$this->propiedade->obtenerPorId($id);
    $this->load->view("header");
    $this->load->view("propiedades/actualizar",$data);
    $this->load->view("footer");
  }

  public function procesarActualizacion(){
      $datosPropiedadesEditado=array(
        "cedula_pro"=>$this->input->post("cedula_pro"),
        "apellidos_pro"=>$this->input->post("apellidos_pro"),
        "nombres_pro"=>$this->input->post("nombres_pro"),
        "celular_pro"=>$this->input->post("celular_pro"),
        "email_pro"=>$this->input->post("email_pro"),
        "latitud1_pro"=>$this->input->post("latitud1_pro"),
        "longitud1_pro"=>$this->input->post("longitud1_pro"),
        "latitud2_pro"=>$this->input->post("latitud2_pro"),
        "longitud2_pro"=>$this->input->post("longitud2_pro"),
        "latitud3_pro"=>$this->input->post("latitud3_pro"),
        "longitud3_pro"=>$this->input->post("longitud3_pro"),
        "latitud4_pro"=>$this->input->post("latitud4_pro"),
       "longitud4_pro"=>$this->input->post("longitud4_pro")
      );
    $id=$this->input->post("id_pro");
    if ($this->propiedade->actualizar($id,$datosPropiedadesEditado)) {
      redirect("propiedades/index");
    }else{
      echo "<h1>NO SE ACTUALIZO<h1>";
    }
  }

  //funcion para renderizar actualizacion formulario
  public function verpropiedad(){
    $data["propiedadeVista"]=$this->propiedade->obtenerTodos();
    $this->load->view("header");
    $this->load->view("propiedades/verpropiedad",$data);
    $this->load->view("footer");
  }

  public function procesarVisualizacion(){
      $datosPropiedadesVisto=array(
        "cedula_pro"=>$this->input->post("cedula_pro"),
        "apellidos_pro"=>$this->input->post("apellidos_pro"),
        "nombres_pro"=>$this->input->post("nombres_pro"),
        "celular_pro"=>$this->input->post("celular_pro"),
        "email_pro"=>$this->input->post("email_pro"),
        "latitud1_pro"=>$this->input->post("latitud1_pro"),
        "longitud1_pro"=>$this->input->post("longitud1_pro"),
        "latitud2_pro"=>$this->input->post("latitud2_pro"),
        "longitud2_pro"=>$this->input->post("longitud2_pro"),
        "latitud3_pro"=>$this->input->post("latitud3_pro"),
        "longitud3_pro"=>$this->input->post("longitud3_pro"),
        "latitud4_pro"=>$this->input->post("latitud4_pro"),
       "longitud4_pro"=>$this->input->post("longitud4_pro")
      );
    $id=$this->input->post("id_pro");
    if ($this->propiedade->verpropiedad($id,$datosPropiedadesVisto)) {
      redirect("propiedades/index");
    }else{
      echo "<h1>NO SE ACTUALIZO<h1>";
    }
  }


}// cierre de la clase
