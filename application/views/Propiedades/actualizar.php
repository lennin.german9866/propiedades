<div class="row">
<div class="col-md-12 text-center well">
<h3><b>Vista propiedad </b></h3>
</div>
<div class="row">
<div class="col-md-12">
  <!-- IMPORTAMOS APY -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB-zl5-tCXGXrFcKrkaoUIyP0xSz7Gdsc&libraries=places&callback=initMap"></script>
  <!-- Latest compiled and minified JavaScript -->
  <?php if($propiedadeEditar):?>

<form id="frm_nuevo_propiedade" class="" enctype="multipart/form-data" action="<?php echo site_url('propiedades/procesarActualizacion'); ?>" method="post">

  <div class="row">
    <div class="col-md-4 text-right">
    <label for="">ID:</label>
    </div>
    <div class="col-md-4">
    <input type="hidden" name="id_pro" value="<?php echo $propiedadeEditar->id_pro;?>">
    </div>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">CÉDULA</label>
    </div>
    <DIV class="col-md-7">
    <input type="number" id="cedula_pro" name="cedula_pro" value="<?php echo $propiedadeEditar->cedula_pro;?>" class="form-control" placeholder="Ingrese numero de cedula" readonly>
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">APELLIDOS</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="apellidos_pro" name="apellidos_pro" value="<?php echo $propiedadeEditar->apellidos_pro;?>" class="form-control" placeholder="Ingrese los dos apellidos" readonly >
    </DIV>
    </div>


    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">NOMBRES</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="nombres_pro" name="nombres_pro" value="<?php echo $propiedadeEditar->nombres_pro;?>" class="form-control" placeholder="Ingrese los dos nombres" readonly>
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">CELULAR</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="celular_pro" name="celular_pro" value="<?php echo $propiedadeEditar->celular_pro;?>" class="form-control" placeholder="Ingrese su telefono"readonly >
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">E-MAIL</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="email_pro" name="email_pro" value="<?php echo $propiedadeEditar->email_pro;?>" class="form-control" placeholder="Ingrese su direccion"readonly >
    </div>
    </div>

    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 1</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD1</label>
    </div>
    <div class="col-md-8">
    <input type="text" name="latitud1_pro" id="latitud1_pro"value="<?php echo $propiedadeEditar->latitud1_pro;?>" class="form-control" placeholder="Ingrese la latitud" readonly >
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">LONGITUD1</label>
    </div>
    <div class="col-md-8">
    <input type="text" name="longitud1_pro" id="longitud1_pro"value="<?php echo $propiedadeEditar->longitud1_pro;?>" class="form-control" placeholder="Ingrese la longitud" readonly >
    </div>
    </div>
    </div>

    </div>

    <br>

    </div>
    </div>


    <br>

    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 2</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD2</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="latitud2_pro" id="latitud2_pro"value="<?php echo $propiedadeEditar->latitud2_pro;?>" class="form-control" placeholder="Ingrese la latitud" readonly required>
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">longitud2</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="longitud2_pro" id="longitud2_pro"value="<?php echo $propiedadeEditar->longitud2_pro;?>" class="form-control" placeholder="Ingrese la longitud" readonly required >
    </div>
    </div>
    </div>

    </div>


    <br>

    </div>


    </div>


    <br>

    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 3</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD3</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="latitud3_pro" id="latitud3_pro"value="<?php echo $propiedadeEditar->latitud3_pro;?>" class="form-control" placeholder="Ingrese la latitud" readonly required>
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">LONGITUD3</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="longitud3_pro" id="longitud3_pro"value="<?php echo $propiedadeEditar->longitud3_pro;?>" class="form-control" placeholder="Ingrese la longitud" readonly required>
    </div>
    </div>
    </div>

    </div>

    <br>

    </div>
    </div>

    <br>

        <div class="row">
        <div class="col-md-6 text-right">
        <label for=""> <h3>COORDENADA N° 4</h3> </label>
        <div class="row">
        <div class="col-md-6">

        <div class="row">
        <div class="col-md-4" style="padding-right:1px;">
        <label for="">LATITUD4</label>
        </div>
        <div class="col-md-8">
        <input type="number" name="latitud4_pro" id="latitud4_pro"value="<?php echo $propiedadeEditar->latitud4_pro;?>" class="form-control" placeholder="Ingrese la latitud" readonly required>
        </div>
        </div>
        </div>

        <div class="col-md-6">
        <div class="row">
        <div class="col-md-4">
        <label for="">LONGITUD4</label>
        </div>
        <div class="col-md-8">
        <input type="number" name="longitud4_pro" id="longitud4_pro"value="<?php echo $propiedadeEditar->longitud4_pro;?>" class="form-control" placeholder="Ingrese la longitud" readonly required >
        </div>
        </div>
        </div>

        </div>
    <center>



    <h1> <b>COORDENADAS VISTA  </b>  </h1>
    </center>
    <div class="container-fluid">
    <div id="mapad" style="height:500px; width:100%; border:2px solid black;">
    </div>
    </div>


    </div>  </div>  </div>


    <script type="text/javascript">
        function initMap(){
          alert('API IMPORTADO EXITOSAMENTE');
          // definiendo una cordenada
          var Direccion1=new google.maps.LatLng("<?php echo $propiedadeEditar->latitud1_pro;?>" , "<?php echo $propiedadeEditar->longitud1_pro;?>");
          var Direccion2=new google.maps.LatLng("<?php echo $propiedadeEditar->latitud2_pro;?>" , "<?php echo $propiedadeEditar->longitud2_pro;?>");
          var Direccion3=new google.maps.LatLng("<?php echo $propiedadeEditar->latitud3_pro;?>" , "<?php echo $propiedadeEditar->longitud3_pro;?>");
          var Direccion4=new google.maps.LatLng("<?php echo $propiedadeEditar->latitud4_pro;?>" , "<?php echo $propiedadeEditar->longitud4_pro;?>");


          //crear mapa
  var triangulo=[Direccion1,Direccion2,Direccion3,Direccion4];

      var  mapa=new google.maps.Map(
          document.getElementById("mapad"),
          {
          	zoom:5,
          	center:{lat:-1.4928987155208189, lng: -78.36193127924689	},
          	mapTypeId:"terrain"
          }
          );


          var marcadord1= new google.maps.Marker({
                  position:Direccion1,
                  map:mapa,
                  title:"direccion1",

                });
                var marcadord2 = new google.maps.Marker({
                        position:Direccion2,
                        map:mapa,
                        title:"direccion2 ",

                      });
                      var marcadord3 = new google.maps.Marker({
                              position:Direccion3,
                              map:mapa,
                              title:"direccion3 ",

                            });
                            var marcadord4 = new google.maps.Marker({
                                    position:Direccion4,
                                    map:mapa,
                                    title:"direccion4",
                                  });

    var poligono=new google.maps.Polygon({
    	paths:triangulo,
    	strokeColor:"#ff0000",
    	strokeOpacity:"0.5",
    	strokeWeight:5,
    	fillColor:"#00f00",
    	fillOpacity:"0.3"
    });
    poligono.setMap(mapa);

    }

    </script>

    </div>

    <div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i>GUARDAR</button>
    <a href="<?php echo site_url('propiedades/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>CANCELAR</a>
    </div>
    </div>


</form>

<?php else: ?>
      <div class="alert  alert-danger">
      <b>No se encotrno la propiedad</b>
      </div>
      <?php endif; ?>
      </div>
      </div>
      </div>
