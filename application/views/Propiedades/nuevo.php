<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Propiedades</title>


    <!-- IMPORTAMOS APY -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB-zl5-tCXGXrFcKrkaoUIyP0xSz7Gdsc&libraries=places&callback=initMap"></script>
    <!-- Latest compiled and minified JavaScript -->

<legend class="text-center">
  <i class="glyphicon glyphicon-useer"></i><b>Agregar propiedades</b>
</legend>

<form id="frm_nuevo_propiedade" class="" enctype="multipart/form-data" action="<?php echo site_url('propiedades/guardarPropiedades'); ?>" method="post">

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">CÉDULA</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="cedula_pro" name="cedula_pro" value="" class="form-control" placeholder="Ingrese numero de cedula" >
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">APELLIDOS</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="apellidos_pro" name="apellidos_pro" value="" class="form-control" placeholder="Ingrese los dos apellidos" >
    </DIV>
    </div>


    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">NOMBRES</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="nombres_pro" name="nombres_pro" value="" class="form-control" placeholder="Ingrese los dos nombres" >
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">CELULAR</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="celular_pro" name="celular_pro" value="" class="form-control" placeholder="Ingrese su telefono" >
    </DIV>
    </div>

    <div class="row">
    <div class="col-md-4 text-right">
    <label for="">E-MAIL</label>
    </div>
    <DIV class="col-md-7">
    <input type="text" id="email_pro" name="email_pro" value="" class="form-control" placeholder="Ingrese su direccion" >
    </div>
    </div>

    <br>
    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 1</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD1</label>
    </div>
    <div class="col-md-8">
    <input type="text" name="latitud1_pro" id="latitud1_pro"value="" class="form-control" placeholder="Ingrese la latitud" readonly >
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">LONGITUD1</label>
    </div>
    <div class="col-md-8">
    <input type="text" name="longitud1_pro" id="longitud1_pro"value="" class="form-control" placeholder="Ingrese la longitud" readonly required>
    </div>
    </div>
    </div>

    </div>

    <br>

    </div>
    <div class="col-md-6">
    <div id="mapa1" style=" height:150px; width:95%; border:2px solid black;" class="">
    </div>
    </div>
    </div>


    <br>

    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 2</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD2</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="latitud2_pro" id="latitud2_pro"value="" class="form-control" placeholder="Ingrese la latitud" readonly required>
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">longitud2</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="longitud2_pro" id="longitud2_pro"value="" class="form-control" placeholder="Ingrese la longitud" readonly required >
    </div>
    </div>
    </div>

    </div>


    <br>

    </div>
    <div class="col-md-6">
    <div id="mapa2" style=" height:150px; width:95%; border:2px solid black;" class="">
    </div>
    </div>

    </div>


    <br>

    <div class="row">
    <div class="col-md-6 text-right">
    <label for=""> <h3>COORDENADA N° 3</h3> </label>
    <div class="row">
    <div class="col-md-6">

    <div class="row">
    <div class="col-md-4" style="padding-right:1px;">
    <label for="">LATITUD3</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="latitud3_pro" id="latitud3_pro"value="" class="form-control" placeholder="Ingrese la latitud" readonly required>
    </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="row">
    <div class="col-md-4">
    <label for="">LONGITUD3</label>
    </div>
    <div class="col-md-8">
    <input type="number" name="longitud3_pro" id="longitud3_pro"value="" class="form-control" placeholder="Ingrese la longitud" readonly required>
    </div>
    </div>
    </div>

    </div>

    <br>

    </div>
    <div class="col-md-6">
    <div id="mapa3" style=" height:150px; width:95%; border:2px solid black;" class="">
    </div>
    </div>

    </div>

    <br>

        <div class="row">
        <div class="col-md-6 text-right">
        <label for=""> <h3>COORDENADA N° 4</h3> </label>
        <div class="row">
        <div class="col-md-6">

        <div class="row">
        <div class="col-md-4" style="padding-right:1px;">
        <label for="">LATITUD4</label>
        </div>
        <div class="col-md-8">
        <input type="number" name="latitud4_pro" id="latitud4_pro"value="" class="form-control" placeholder="Ingrese la latitud" readonly required>
        </div>
        </div>
        </div>

        <div class="col-md-6">
        <div class="row">
        <div class="col-md-4">
        <label for="">LONGITUD4</label>
        </div>
        <div class="col-md-8">
        <input type="number" name="longitud4_pro" id="longitud4_pro"value="" class="form-control" placeholder="Ingrese la longitud" readonly required >
        </div>
        </div>
        </div>

        </div>

        <br>

        </div>
        <div class="col-md-6">
        <div id="mapa4" style=" height:150px; width:95%; border:2px solid black;" class="">
        </div>
        </div>

        </div>



            <script type="text/javascript">
                function initMap(){
                  alert('API IMPORTADO EXITOSAMENTE');
                  // definiendo una cordenada
                  var latitud_longitud1 =new google.maps.LatLng(-0.5154459632308808, -78.56367111374846);
                  var latitud_longitud2 =new google.maps.LatLng(-0.532149961676703, -78.57199242855575);
                  var latitud_longitud3 =new google.maps.LatLng(-0.532149961676703, -78.57199242855575);
                  var latitud_longitud4 =new google.maps.LatLng(-0.532149961676703, -78.57199242855575);
                  //crear mapa
                  var mapa1=new google.maps.Map(
                    document.getElementById('mapa1'),
                    {
                      center:latitud_longitud1,
                      zoom:15,
                      mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                  );

                  var mapa2=new google.maps.Map(
                    document.getElementById('mapa2'),
                    {
                      center:latitud_longitud2,
                      zoom:15,
                      mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                  );
                  var mapa3=new google.maps.Map(
                    document.getElementById('mapa3'),
                    {
                      center:latitud_longitud3,
                      zoom:15,
                      mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                  );
                  var mapa4=new google.maps.Map(
                    document.getElementById('mapa4'),
                    {
                      center:latitud_longitud4,
                      zoom:15,
                      mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                  );

                  var marcador1= new google.maps.Marker({
                          position: latitud_longitud1,
                          map:mapa1,
                          title:"Seleccione la direccion1",
                          draggable:true
                        });
                        var marcador2 = new google.maps.Marker({
                                position: latitud_longitud2,
                                map:mapa2,
                                title:"Seleccione la direccion2 ",
                                draggable:true
                              });
                              var marcador3 = new google.maps.Marker({
                                      position: latitud_longitud3,
                                      map:mapa3,
                                      title:"Seleccione la direccion3 ",
                                      draggable:true
                                    });
                                    var marcador4 = new google.maps.Marker({
                                            position: latitud_longitud4,
                                            map:mapa4,
                                            title:"Seleccione la direccion4",
                                            draggable:true
                                          });
                        google.maps.event.addListener(
                          marcador1,
                          'dragend',
                          function(event){
                            var latitud1=this.getPosition().lat();
                            var longitud1=this.getPosition().lng();
                            alert("LATITUD:"+latitud1);
                            alert("LONGITUD:"+longitud1);
                            document.getElementById("latitud1_pro").value=latitud1;
                            document.getElementById("longitud1_pro").value=longitud1;
                          }
                        );
                        google.maps.event.addListener(
                          marcador2,
                          'dragend',
                          function(event){
                            var latitud2=this.getPosition().lat();
                            var longitud2=this.getPosition().lng();
                            alert("LATITUD:"+latitud2);
                            alert("LONGITUD:"+longitud2);
                            document.getElementById("latitud2_pro").value=latitud2;
                            document.getElementById("longitud2_pro").value=longitud2;
                          }
                        );
                        google.maps.event.addListener(
                          marcador3,
                          'dragend',
                          function(event){
                            var latitud3=this.getPosition().lat();
                            var longitud3=this.getPosition().lng();
                            alert("LATITUD:"+latitud3);
                            alert("LONGITUD:"+longitud3);
                            document.getElementById("latitud3_pro").value=latitud3;
                            document.getElementById("longitud3_pro").value=longitud3;
                          }
                        );
                        google.maps.event.addListener(
                          marcador4,
                          'dragend',
                          function(event){
                            var latitud4=this.getPosition().lat();
                            var longitud4=this.getPosition().lng();
                            alert("LATITUD:"+latitud4);
                            alert("LONGITUD:"+longitud4);
                            document.getElementById("latitud4_pro").value=latitud4;
                            document.getElementById("longitud4_pro").value=longitud4;
                          }
                        );
                      }
                  </script>

    <div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i>GUARDAR</button>
    <a href="<?php echo site_url('propiedades/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>CANCELAR</a>
    </div>
    </div>

</form>

<script type="text/javascript">
     $("#frm_nuevo_propiedade").validate({
        rules:{
            apellidos_pro:{
              required:true,
              minlength:3
            },
            cedula_pro:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            nombres_pro:{
              required:true,
              minlength:3
            },
            celular_pro:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
            email_pro:{
              required:true
            },
        },
        messages:{
            apellidos_pro:{
              required:"Por favor complete los apellidos",
              minlength:"Apellido Incorrecto"
            },
            cedula_pro:{
              required:"Por favor ingrese la cédula",
              minlength:"Cédula Incorrecta",
              maxlength:"Cédula Incorrecta",
              digits:"Este campo solo acepta números"
            },
            nombres_pro:{
              required:"Por favor ingrese sus nombre",
              minlength:"Nombre incorrecto"
            },
            celular_pro:{
              required:"Por favor ingrese su telefono celular",
              minlength:"Telefono incorrecto",
              maxlength:"Telefono incorrecto",
              digits:"Este campo solo acepta números"
            },
            email_pro:{
              required:"Por favor ingrese la dirección"
            },
        }
     });
</script>
