<legend class="text-center"> <i class="glyphicon glyphicon-hand-right"></i> <b>Propiedades agregadas</b>
  <div class="row text-center">
    <div class="col-md-12">
    </div>
      </button>
   <a href="<?php echo site_url("propiedades/nuevo") ?>" class="btn btn-danger"> <i class="glyphicon glyphicon-screenshot"></i> Agregar </a>

    </div>
</legend>
<hr>
<?php if ($listadoPropiedades): ?>


  <table id="tbl-popiedades" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Cedula</th>
      <th class="text-center">Apellidos</th>
      <th class="text-center">Nombres</th>
      <th class="text-center">Celular</th>
      <th class="text-center">E-mail</th>
      <th class="text-center">Latitud1</th>
      <th class="text-center">Longitud1</th>
      <th class="text-center">Latitud2</th>
      <th class="text-center">Longitud2</th>
      <th class="text-center">Latitud3</th>
      <th class="text-center">Longitud3</th>
      <th class="text-center">Latitud4</th>
      <th class="text-center">Longitud4</th>
      <th class="text-center">Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoPropiedades->result() as $propiedadeTemporal): ?>
      <tr>
        <td class="text-center"><?php echo $propiedadeTemporal->id_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->cedula_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->apellidos_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->nombres_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->celular_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->email_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->latitud1_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->longitud1_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->latitud2_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->longitud2_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->latitud3_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->longitud3_pro ?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->latitud4_pro?></td>
        <td class="text-center"> <?php echo $propiedadeTemporal->longitud4_pro?></td>
        <td class="text-center">
        <a  href="<?php echo site_url("propiedades/actualizar");?>/<?php echo $propiedadeTemporal->id_pro;?>"class="btn btn-warning">
        <i class="glyphicon glyphicon-edit"></i>Ver propiedad</a>
        <a  href="<?php echo site_url("propiedades/verpropiedad");?>/<?php echo $propiedadeTemporal->id_pro;?>"class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>PROPIEDADEST </a>
        <a  href="<?php echo site_url("propiedades/borrar");?>/<?php echo $propiedadeTemporal->id_pro;?>"class="btn btn-danger" onclick="return confirm('Esta seguro de eliminar');">
        <i class="glyphicon glyphicon-trash" ></i>Elimiar</a>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe propiedades</b></h3>
<?php endif; ?>

  <script>
