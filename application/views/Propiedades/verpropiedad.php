<div class="row">
<div class="col-md-12 text-center well">
<h3><b> </b></h3>
</div>
<div class="row">
<div class="col-md-12">
  <!-- IMPORTAMOS APY -->
  <!-- Latest compiled and minified JavaScript -->
    <h1 class="text-center"> <b> PROPIEDADES  </b>  </h1>
    </center>
    <div class="container-fluid">
    <div id="mapat" style="height:500px; width:100%; border:2px solid black;">
    </div>
    </div>


    </div>  </div>  </div>
<script type="text/javascript">

function initMap (){

  // llamar a todas las listas creadas
  var latitud_longitud= new google.maps.LatLng(-0.511815969301196, -78.56325197035302);
  var mapa = new google.maps.Map(document.getElementById('mapat'),
  {
    center:latitud_longitud,
    zoom:15,
    mapTypeId:'terrain',
  }
  );
  //Crear un foreach para graficar todos los mapas que se vayan registrando
  <?php foreach ($propiedadeVista->result() as $fila): ?>
    latitud_longitud1=new google.maps.LatLng(<?php echo $fila->latitud1_pro ?>,<?php echo $fila->longitud1_pro ?>);
    latitud_longitud2=new google.maps.LatLng(<?php echo $fila->latitud2_pro ?>,<?php echo $fila->longitud2_pro ?>);
    latitud_longitud3=new google.maps.LatLng(<?php echo $fila->latitud3_pro ?>,<?php echo $fila->longitud3_pro ?>);
    latitud_longitud4=new google.maps.LatLng(<?php echo $fila->latitud4_pro ?>,<?php echo $fila->longitud4_pro; ?>);
    var triangulo=[latitud_longitud1,latitud_longitud2,latitud_longitud3,latitud_longitud4];

    var marcador = new google.maps.Marker({
      position: latitud_longitud1,
      map:mapa,
      title: ""
    });
    var marcador = new google.maps.Marker({
      position: latitud_longitud2,
      map:mapa,
      title: ""
    });
    var marcador = new google.maps.Marker({
      position: latitud_longitud3,
      map:mapa,
      title: ""
    });
    var marcador = new google.maps.Marker({
      position: latitud_longitud4,
      map:mapa,
      title: ""
    });
    var poligono=new google.maps.Polygon({
        paths:triangulo,
        strokeColor:"#000000",//color de la linea o borde
        strokeOpacity:"0.5",//para que se vea lo que esta atras
        strokeWeight:3,
        fillColor:"#28415f",
        fillOpacity:"0.5",
    });
                            //segunda forma para visualizar el poligono
    poligono.setMap(mapa);

  <?php endforeach; ?>//Fin de la grafica ver todos los mapas


} //fin de la funcion InitMap
</script>
